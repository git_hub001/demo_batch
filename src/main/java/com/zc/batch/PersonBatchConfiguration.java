package com.zc.batch;

import com.zc.model.Person;
import com.zc.service.UserService;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.batch.MyBatisBatchItemWriter;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ClassPathResource;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

/**
 * 处理具体工作业务  主要包含三个部分:读数据、处理数据、写数据
 * @author wbw
 *
 */
//@Configuration
//@EnableBatchProcessing
public class PersonBatchConfiguration {
    //插入语句
    // tag::readerwriterprocessor[] 1.读数据

    @Autowired
    private UserService service ;

    @Autowired
    private DataSource primaryDataSource ;

    @Autowired
    private SqlSessionFactory sqlSessionFactory ;


    //@Bean
    public ItemReader<Person> reader() {
        FlatFileItemReader<Person> reader = new FlatFileItemReader<Person>();
        //加载外部文件数据 文件类型:CSV
        reader.setResource(new ClassPathResource("euzd_20170724001.csv"));
        reader.setLineMapper(new DefaultLineMapper<Person>() {{
            setLineTokenizer(new DelimitedLineTokenizer() {{
                setNames(new String[] { "personName","personAge","personSex" });
            }});
            setFieldSetMapper(new BeanWrapperFieldSetMapper<Person>() {{
                setTargetType(Person.class);
            }});
        }});
        return reader;
    }
    //2.处理数据
    //@Bean
    public PersonItemProcessor processor() {
        return new PersonItemProcessor();
    }
    //3.写数据
    //@Bean
    public ItemWriter<Person> writer() {


//        JdbcBatchItemWriter<Person> writer = new JdbcBatchItemWriter<Person>();
//        writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<Person>());
//        writer.setSql(PERSON_INSERT);
//        writer.setDataSource(getDataSource());

        MyBatisBatchItemWriter<Person> writer = new MyBatisBatchItemWriter<Person>();
        writer.setSqlSessionFactory(sqlSessionFactory);
        String statementId = "com.zc.mapper.PersonMapper.insert";
        writer.setStatementId(statementId);
        CompositeItemWriter compositeItemWriter  = new CompositeItemWriter();
        List delegates = new ArrayList();
        delegates.add(writer);
        compositeItemWriter.setDelegates(delegates);
        writer.setAssertUpdates(false);
        return writer;
    }
    // end::readerwriterprocessor[]


    // tag::jobstep[]
   // @Bean
    public Job importUserJob(JobBuilderFactory jobs, @Qualifier("step1")Step s1, JobExecutionListener listener) {
        return jobs.get("importUserJob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(s1)
                .end()
                .build();
    }

  //  @Bean
    public Step step1(StepBuilderFactory stepBuilderFactory, ItemReader<Person> reader,
                      ItemWriter<Person> writer, ItemProcessor<Person, Person> processor) {
        return stepBuilderFactory.get("step1")
                .<Person, Person> chunk(10)
                .reader(reader)
                .processor(processor)
                .writer(writer)
                .build();
    }
    // end::jobstep[]

}
