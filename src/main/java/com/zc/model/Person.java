package com.zc.model;

public class Person {
    private Integer personId;

    private String personName;

    private String personAge;

    private String personSex;

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName == null ? null : personName.trim();
    }

    public String getPersonAge() {
        return personAge;
    }

    public void setPersonAge(String personAge) {
        this.personAge = personAge == null ? null : personAge.trim();
    }

    public String getPersonSex() {
        return personSex;
    }

    public void setPersonSex(String personSex) {
        this.personSex = personSex == null ? null : personSex.trim();
    }

    public Person(String personName, String personAge, String personSex) {
        this.personName = personName;
        this.personAge = personAge;
        this.personSex = personSex;
    }

    public Person() {
    }
}